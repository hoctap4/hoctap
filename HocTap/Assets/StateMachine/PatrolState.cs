using UnityEngine.AI;
using UnityEngine;

[CreateAssetMenu(menuName ="StateSO/Patrol")]
public class PatrolState : StateSO
{
    [SerializeField] NPCMovementConfigSO NPCMovementConfigSO;
    public override void OnChangeState(StateController stateController)
    {
         
    }

    public override void OnEnter(StateController stateController)
    {
        stateController.Agent.isStopped = false;
        stateController.AnimatorController.SetBool("Patrol", true);
        stateController.Agent.speed = NPCMovementConfigSO.MoveSpeed;
    }

    public override void OnExit(StateController stateController)
    {
        stateController.AnimatorController.SetBool("Patrol", false);
    }

    public override void OnUpdate(StateController stateController)
    {
      
        if (IsCompletePath(stateController.Agent))
        {
            stateController.ChangeState(typeof(IdleState)); 
        }

        if (stateController.EnemyController.Player!=null)
        {
            stateController.ChangeState(typeof(ChaseState));
        }
    }


    private bool IsCompletePath(NavMeshAgent agent)
    {
        if (!agent.pathPending)
        {
            if (agent.stoppingDistance == 0) agent.stoppingDistance = 0.1f;
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
