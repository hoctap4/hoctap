using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="NPC Config")]

public class NPCMovementConfigSO : ScriptableObject
{
    [SerializeField] private float stopDuration;
    [SerializeField] private float moveSpeed;
    [SerializeField] private float range;

    public float StopDuration => stopDuration;
    public float MoveSpeed => moveSpeed;
    public float Range => range;

}
