using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="StateSO/ChaseState")]
public class ChaseState : StateSO
{
    public override void OnChangeState(StateController stateController)
    {

    }

    public override void OnEnter(StateController stateController)
    {
        stateController.Agent.isStopped = false;
        stateController.AnimatorController.SetBool("Chase", true);
    }

    public override void OnExit(StateController stateController)
    {
        stateController.AnimatorController.SetBool("Chase", false);
      
    }

    public override void OnUpdate(StateController stateController)
    {
        if (stateController.EnemyController.Player != null)
        {
            //
            stateController.Agent.SetDestination(stateController.EnemyController.Player.transform.position);
        }
        else
        {  stateController.Agent.isStopped = true;
            //stateController.Agent.SetDestination(Vector3.zero);
            stateController.ChangeState(typeof(IdleState));
        }
    }
}
