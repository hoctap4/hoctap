using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System.Linq;

public class StateController: MonoBehaviour 
{
    //Debug 
  
    //
    private NavMeshAgent agent;
    private SphereCollider sphereCollider;
    public SphereCollider SphereCollider=>sphereCollider;
    public NavMeshAgent Agent=>agent;

    private Enemy enemyController;
    public Enemy EnemyController => enemyController;
    public StateSO currentState;
    [SerializeField] private List<StateSO> stateSOs;
    private Animator animatorController;
    public Animator AnimatorController => animatorController;


    private void Start()
    {
        animatorController = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        sphereCollider = GetComponent<SphereCollider>();
        enemyController = GetComponent<Enemy>();
        ChangeState(typeof(IdleState));
    }
    private void Update()
    {
        if (currentState != null)
        {
            currentState.OnUpdate(this);
        }
    }

    public void ChangeState(Type newState)
    {
        if (currentState != null)
        {
            currentState.OnExit(this);
        }
        currentState = stateSOs.First(x=>x.GetType()==newState);
        currentState.OnEnter(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            ChangeState(typeof(ChaseState));
    }
    private void OnTriggerExit(Collider other)
    {
          if (other.CompareTag("Player"))
            ChangeState(typeof(IdleState));
    }


}


