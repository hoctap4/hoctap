using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

enum State
{ 
    Idle,
    Chase,
    Patrol
}
public class CharacterTest : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<Transform> wayPoints;
   

    Animator animator;
    NavMeshAgent _agent;
    SphereCollider sphereCollider;
    State state;
    int wayPointIndex=-1;

    float waitTime;


    public float range;
    public Transform centrePoint;

    void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        sphereCollider = GetComponent<SphereCollider>();
        state = State.Idle;
        animator = FindAnyObjectByType<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        { 
            case State.Idle:
                
                if (waitTime >= 0.75f)
                {
                    Vector3 point;
                    if (GetRandomPosition(centrePoint.position, range, out point))
                    { 
                         waitTime = 0;
                        state = State.Patrol;
                        animator.SetBool("Chase", true);
                        Debug.DrawRay(point, Vector3.up, Color.blue, 1.0f); //so you can see with gizmos
                        _agent.SetDestination(point);
                    }
                   
                   // _agent.isStopped = false;
                }
                waitTime += Time.deltaTime;
                break;

            case State.Chase:
                break;
           
            case State.Patrol:
                if (Statement())
                {
                    // _agent.isStopped = true;
                    animator.SetBool("Chase", false);
                    state = State.Idle;
                }
                

               
                break;

        }

      
    }
    int currentIndex = 0;
    int lastIndex = 0;

    //patrol
    public Vector3 GetNextPosisiton()
    {
        Vector3 nextDestination = _agent.transform.position;

        if (wayPoints.Count > 0)
        {
            do
            {
                currentIndex = Random.Range(0, wayPoints.Count);
            }
            while (currentIndex == lastIndex);
           
            nextDestination = wayPoints[currentIndex].position;
        }
        lastIndex = currentIndex;
        return nextDestination;
    }


    public bool GetRandomPosition(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = _agent.transform.position + Random.insideUnitSphere * range;
        NavMeshHit hit;

        if (NavMesh.SamplePosition(randomPoint, out hit, 1, NavMesh.AllAreas))
        {
            result = hit.position;
            return true;
        }
        result = Vector3.zero;
        return false;
    }
    private bool Statement()
    {
        if (!_agent.pathPending)
        {
            if (_agent.stoppingDistance == 0) _agent.stoppingDistance = 0.1f;
            if (_agent.remainingDistance <= _agent.stoppingDistance)
            {
                if (!_agent.hasPath || _agent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
