using System;
using UnityEngine;

public abstract class StateSO : ScriptableObject
{
    public abstract void OnEnter(StateController stateController);
    public abstract void OnUpdate(StateController stateController);
    public abstract void OnChangeState(StateController stateController);

    public abstract void OnExit(StateController stateController);

}
