using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
[Serializable]
class BoolEvent : UnityEvent<bool, GameObject> { };
public class EnemyTrigger : MonoBehaviour
{
    [SerializeField] private BoolEvent enterZone;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("sadf");
            enterZone?.Invoke(true, other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
         if (other.CompareTag("Player"))
        {
            enterZone?.Invoke(false, other.gameObject);
        }
    }
}
