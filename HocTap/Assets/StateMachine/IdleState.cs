using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[CreateAssetMenu(menuName = "StateSO/IdleState")]
public class IdleState : StateSO
{
    [SerializeField] NPCMovementConfigSO NPCMovementConfigSO;
    private float countTime;


    public override void OnEnter(StateController stateController)
    {
       // stateController.Agent.isStopped = false;
        countTime = 0;
        //stateController.AnimatorController.SetBool("Idle",true); khong can set
    }

    public override void OnUpdate(StateController stateController)
    {
        countTime += Time.deltaTime;
        if (countTime >= NPCMovementConfigSO.StopDuration)
        {
            if (GetRandomPosition(stateController.Agent.transform.position, NPCMovementConfigSO.Range, out Vector3 randomPoint))
            {
                Debug.DrawRay(randomPoint, Vector3.up, Color.blue, 1.0f);
                stateController.Agent.SetDestination(randomPoint);
                stateController.ChangeState(typeof(PatrolState));

            }
        }

        if (stateController.EnemyController.Player)
        {
            stateController.ChangeState(typeof(ChaseState));
        }
    }
    
    public override void OnExit(StateController stateController)
    {
      //  stateController.AnimatorController.SetBool("Idle", false);
    }

    public override void OnChangeState(StateController stateController)
    {
        throw new System.NotImplementedException();
    }
    private bool GetRandomPosition(Vector3 center, float range, out Vector3 result)
    {
        Vector3 randomPoint = center + Random.insideUnitSphere * range;
        NavMeshHit navMeshHit;
        Debug.Log("randomPointOut " + randomPoint);
        if (NavMesh.SamplePosition(randomPoint, out navMeshHit, 1, NavMesh.AllAreas))
        {
            Debug.Log("randomPointIn " + randomPoint);
            result = navMeshHit.position;
            return true;
        }
        result = Vector3.zero;
        return false;


    }



}
