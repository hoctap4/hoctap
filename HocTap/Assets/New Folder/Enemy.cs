using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public bool isPlayerChaseZone;
    private Player player;
    public Player Player => player;
    public void OnChaseTriggerChange(bool entered, GameObject who)
    {
        isPlayerChaseZone = entered;
        if (entered && who.TryGetComponent<Player>(out Player p))
        {
            player = p;
        }
        else
        {
            player = null;
        }
    }


}
